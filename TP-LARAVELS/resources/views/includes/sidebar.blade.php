<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
   -->
    <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
        Creative Tim
      </a></div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item active  ">
          <a class="nav-link" href="{{url('/dashbord')}}">
            <i class="material-icons">dashboard</i>
            <p>Tableau de bord</p>
          </a>
        </li>
        <!--
            <li class="nav-item ">
          <a class="nav-link" href="./user.html">
            <i class="material-icons">person</i>
            <p>User Profil</p>
          </a>
        </li>
        -->

        <li class="nav-item ">
          <a class="nav-link" href="{{url('/pays')}}">
            <i class="material-icons">content_paste</i>
            <p>Pays</p>
          </a>
        </li>

        <li class="nav-item ">
          <a class="nav-link" href="{{url('/notif')}}">
            <i class="material-icons">notifications</i>
            <p>Notifications</p>
          </a>
        </li>
      </ul>
    </div>
</div>

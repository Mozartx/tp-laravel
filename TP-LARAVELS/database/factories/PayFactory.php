<?php

namespace Database\Factories;

use App\Models\Pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->country,
            'description' => $this->faker->sentence,
            'code_indicatif' => $this->faker->countryCode,
            'continent' => $this->faker->randomElement(["Afrique", "Asie", "Europe", "Amérique", "Australie"]),
            'population' => $this->faker->randomElement([25000303, 3000000, 5000000, 753453, 200000]),
            'capitale' => $this->faker->city,
            'monnaie' => $this->faker->randomElement(["XOR", "EUR", "DOLLAR"]),
            'langue' => $this->faker->randomElement(["FR", "EN", "AR","ES"]),
            'superficie' => $this->faker->randomElement([200000.0, 300457.0, 500000.0, 750565.0, 250098.0]),
            'est_laique' => $this->faker->randomElement([true, false])
        ];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Creation de la table pays
        Schema::create('pays', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->string('description')->nullable();
            $table->string('code_indicatif')->nullable();
            $table->string('continent')->nullable();
            $table->integer('population')->nullable();
            $table->string('capitale');
            $table->string('monnaie');
            $table->string('langue')->nullable();
            $table->double('superficie')->nullable();
            $table->boolean('est_laique')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

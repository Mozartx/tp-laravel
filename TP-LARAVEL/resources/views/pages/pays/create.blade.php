@extends('layouts.main')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
            @if (session('message'))
                <div class="alert alert-{{session('statut')? 'success':'danger'}}">
                    {{session('message')}}
                </div>
            @endif
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Enregistrement d'un pays</h4>
            </div>
            <div class="card-body">
                <form action="{{route('pays.store')}}" method="POST">
                    @method("POST")
                    @csrf
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group">
                      <label class="bmd-label-floating">Pays</label>
                      <input type="text" class="form-control" name="libelle" >
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label class="bmd-label-floating">Code indicatif</label>
                      <input type="text" class="form-control" name="code_indicatif" >
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="bmd-label-floating">Continent</label>
                      <input type="text" class="form-control" name="continent">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Langue</label>

                      <select name="langue" class="form-control">
                        <option value ="FR">FR</option>
                        <option value="EN">EN</option>
                        <option value="AR">AR</option>
                        <option value="ES">ES</option>
                    </select>

                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Capitale</label >
                      <input type="text" class="form-control" name="capitale" >
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Description</label>
                      <input type="text" class="form-control" name="description">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="bmd-label-floating">Superficie</label>
                      <input type="Number" class="form-control"  name="superficie" >
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="bmd-label-floating">Monnaie</label>

                      <select name="monnaie" class="form-control" >
                        <option value ="XOR">XOR</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>

                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="bmd-label-floating">Est laique</label>
                      <select name="est_laique" class="form-control" type="number" >
                        <option value ="1">Oui</option>
                        <option value="0">Non</option>
                    </select>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
                <a class="btn btn-default float-right" href="{{url('/pays/create')}}">Annuler</a>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('includes.footer')
@endsection
